SELECT
IL.PurchaseOrderHeader_DocumentNbr AS [PO_number],
IL.POLineNbr AS [PO_line_number]
INTO #MatchException
FROM VendorInvoiceLine AS IL
INNER JOIN PurchaseOrderLine AS POL
ON IL.PurchaseOrderHeader_DocumentNbr = POL.FK_DocumentNbr
AND IL.POLineNbr = POL.LineNbr
INNER JOIN VendorInvoiceHeader AS IH
ON IL.FK_DocumentNbr = IH.DocumentNbr
INNER JOIN Vendor AS V
ON IH.FK_VendorID = V.ID
INNER JOIN VendorsCustomer AS VC
ON V.ID = VC.FK_VendorID
WHERE IH.Status = 'Match Exception'
-- Vacations vendors only
AND LEFT(VC.CustVendorNbr, 1) = '8'

SELECT
IL.FK_DocumentNbr AS [invoice_document_number],
VC.CustVendorNbr AS [vendor_number],
V.Name AS [vendor_name],
IH.InvoiceNbr AS [invoice_number],
IH.SubmitDate As [invoice_submit_date],
IH.DN_PassEditsFlag AS [invoice_pass_edits],
IL.LineNbr AS [invoice_line_number],
IL.PurchaseOrderHeader_DocumentNbr AS [PO_number],
IL.POLineNbr AS [PO_line_number]
FROM VendorInvoiceLine AS IL
INNER JOIN #MatchException
ON IL.PurchaseOrderHeader_DocumentNbr = #MatchException.PO_number
AND IL.POLineNbr = #MatchException.PO_line_number
INNER JOIN VendorInvoiceHeader AS IH
ON IL.FK_DocumentNbr = IH.DocumentNbr
INNER JOIN Vendor AS V
ON IH.FK_VendorID = V.ID
INNER JOIN VendorsCustomer AS VC
ON V.ID = VC.FK_VendorID