# InvoiceWorks Match Exception

## Overview

There are some Purchase Orders from Vacation vendors receiveing duplicate invoices and when this happens it puts the invoices in Match Exception Status.

This project will identify duplicate invoices with the Match Exception status. A Spotfire Report will be generated that shows:

* PO #
* Invoice # against PO #
* Invoice Status
* Invoice Submitted Date
* Vendor #

The contact for this project is Erika Hopes.

Team: Accounts Payable, Vacations

### Filepaths

| Description | Path |
| --- | --- |
| InvoiceWorks Database Reference | \\sscdfs_new\SharedFile\payments\Data Table References |
| Match exception report download target | \\sscdfs_new\sharedfile\fbi\invoiceworks-match-exception\data\match-exception-report.csv |
| Spotfire Library Path | /library/fbi/invoiceworks-match-exception/ |
| Spotfire Analysis URL | https://reportsinternal.jetblue.com/spotfire/wp/OpenAnalysis?file=/fbi/vacations-duplicate-invoices/vacation-duplicate-invoices |

## Getting started

### Prerequisites
- Python 3.7 (with pip)
- Selenium Internet Explorer webdriver
- InvoiceWorks account
- TIBCO Spotfire Client

### Installation

```bash
# clone the repository
$ git clone git@bitbucket.org:jetbluefbi/invoiceworks-match-exception.git
$ cd invoiceworks-match-exception
$ pip install .
```

### Usage

```bash
# run the CLI
$ iw-match-exception
```

