from setuptools import setup, find_packages

setup(
    name="iwmatchexception",
    version="2018.10.04",
    author='Tyler Martinez',
    author_email='tyler.martinez@jetblue.com',
    packages=find_packages(),
    install_requires=['pandas', 'requests', 'selenium'],
    entry_points={
        'console_scripts': [
            'iw-possible-duplicates = iwmatchexception.jtpduplicates:main'
        ]
    }
)
