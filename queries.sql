/* Queries for Ad-Hoc reporting tool @ https://archive.invoiceworks.net */

/* Invoice header */
SELECT
H.DocumentNbr AS [document_number],
H.FK_VendorID AS [vendor_id],
H.InvoiceNbr AS [invoice_number],
H.InvoiceDate AS [invoice_date],
H.SubmitDate AS [submitted_date],
H.DN_TotalDue AS [total_amount],
H.DN_PassEditsFlag AS [pass_edits],
V.Name AS [vendor_name],
VC.CustVendorNbr AS [vendor_number],
H.Status AS [status]
FROM VendorInvoiceHeader AS H
INNER JOIN VendorsCustomer AS VC
ON H.FK_VendorID = VC.FK_VendorID
INNER JOIN Vendor AS V
ON H.FK_VendorID = V.ID
WHERE H.Status = 'Match Exception'


/* Invoice line item */
SELECT
L.FK_DocumentNbr AS [invoice_document_number],
L.LineNbr AS [line_number],
L.POLineNbr AS [purchase_order_line_number],
L.Quantity AS [quantity],
L.Description AS [description],
L.LineTotal_Base AS [total_amount],
L.UnitCost_Base AS [unit_cost],
L.TaxAmount_Base AS [tax_amount],
L.PurchaseOrderHeader_DocumentNbr AS [purchase_order_document_number]
FROM VendorInvoiceLine AS L
INNER JOIN VendorInvoiceHeader AS H
ON L.FK_DocumentNbr = H.DocumentNbr
WHERE H.Status = 'Match Exception'

/* Purchase order header */
SELECT
POH.DocumentNbr AS [purchase_order_document_number],
POH.FK_VendorID AS [vendor_id],
POH.PurchaseOrderNbr AS [purchase_order_number],
L.LineNbr AS [line_number],
L.FK_DocumentNbr AS [invoice_document_number]
FROM PurchaseOrderHeader AS POH
INNER JOIN VendorInvoiceLine AS L
ON L.PurchaseOrderHeader_DocumentNbr = POH.DocumentNbr
INNER JOIN VendorInvoiceHeader AS H 
ON L.FK_DocumentNbr = H.DocumentNbr
WHERE H.Status = 'Match Exception'


/* Purchase order line item */
SELECT
POL.FK_DocumentNbr AS [purchase_order_document_number],
POL.LineNbr AS [line_number],
POL.Quantity AS [quantity],
POL.Description AS [description],
POL.UnitCost AS [unit_cost],
POL.DN_LineTotal AS [document_number_line_total],
POL.QuantityReceived AS [quantity_received],
POL.QuantityInvoiced AS [quantity_invoiced],
POL.Status AS [status]
FROM PurchaseOrderLine AS POL
INNER JOIN VendorInvoiceLine AS L
ON L.PurchaseOrderHeader_DocumentNbr = POL.FK_DocumentNbr
INNER JOIN VendorInvoiceHeader AS H 
ON L.FK_DocumentNbr = H.DocumentNbr
WHERE H.Status = 'Match Exception'

/* Invoice edit error */
SELECT
IE.Invoice_DocumentNbr AS [invoice_document_number],
IE.CustomerEdit_ID AS [customer_edit_id],
IE.EditCheckTS AS [edit_check_timestamp]
FROM InvoiceEditError AS IE
INNER JOIN VendorInvoiceLine AS L
ON IE.Invoice_DocumentNbr = L.FK_DocumentNbr
INNER JOIN VendorInvoiceHeader AS H 
ON L.FK_DocumentNbr = H.DocumentNbr
WHERE H.Status = 'Match Exception'


SELECT
H.InvoiceType AS [invoice_type],
H.Status AS [invoice_status],
L.FK_DocumentNbr AS [invoice_document_number],
L.LineNbr AS [line_number],
L.POLineNbr AS [purchase_order_line_number],
L.Quantity AS [quantity],
L.Description AS [description],
L.LineTotal_Base AS [total_amount],
L.UnitCost_Base AS [unit_cost],
L.TaxAmount_Base AS [tax_amount],
L.Status AS [status],
L.PurchaseOrderHeader_DocumentNbr AS [purchase_order_document_number],
CE.Scope AS [error_scope],
CE.ErrorMsg AS [error_message],
CE.FieldXPath AS [error_field_x_path],
CE.Type AS [error_type]
FROM VendorInvoiceLine AS L
INNER JOIN VendorInvoiceHeader AS H
ON L.FK_DocumentNbr = H.DocumentNbr
Inner JOIN InvoiceEditError AS IE
ON L.FK_DocumentNbr = IE.Invoice_DocumentNbr
INNER JOIN CustomerEdit AS CE
ON IE.CustomerEdit_ID = CE.ID
WHERE L.FK_DocumentNbr = 1028028958
ORDER BY L.LineNbr ASC