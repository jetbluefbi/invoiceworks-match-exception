from io import StringIO
from pathlib import Path

import pandas as pd

from .iwbots import IWArchiveBot
# from iwmatchexception.iwbots import IWArchiveBot


REPORT_NAME = 'vacations_possible_duplicate_invoices'
REPORT_ID = 232
SAVE_FILEPATH = '//sscdfs_new/sharedfile/fbi/invoiceworks-match-exception/possible-duplicates.csv'

def main():
    # Get Report
    abot = IWArchiveBot()
    abot.connect()
    r = abot.export_report_as_csv(REPORT_ID)
    # clean report data
    content = r.content
    content_as_str = content.decode()
    content_as_file_obj = StringIO(content_as_str)
    df = pd.read_csv(content_as_file_obj)
    df = df.iloc[:, :-1]
    df.drop_duplicates(inplace=True)
    # save report data
    fp = Path(SAVE_FILEPATH).resolve()
    df.to_csv(fp, index=False)

if __name__ == '__main__':
    main()