import warnings
from urllib3.exceptions import InsecureRequestWarning

import requests
from selenium import webdriver


class IWBot():
    """A bot used for https://airlines.invoiceworks.net.

    Attributes
    ----------
        authenticated : bool
            True if the session is authenticated with InvoiceWorks.
        driver : selenium.webdriver.ie.webdriver.WebDriver
        session : requests.Session
    """

    def __init__(self):
        self.authenticated = False
        self.driver = webdriver.Ie()
        self.session = requests.Session()

    @property
    def verify_ssl(self):
        return self.session.verify
    
    @verify_ssl.setter
    def verify_ssl(self, value):
        if not isinstance(value, bool):
            raise TypeError('`value` must be a bool.')   
        self.session.verify = value
        action = 'ignore' if value else 'default'
        warnings.simplefilter(action, category=InsecureRequestWarning)

    def connect(self):
        """Authenticate the `IWBot` object with invoiceworks.net using
        hellojetblue.com's SSO feature as middleware.

        Must use `webdriver.Ie` for SSO to work.
        """
        sso_url = 'https://www.hellojetblue.com/resources/invoiceworks'
        self.driver.get(sso_url)
        self.set_session_cookies_from_driver()
        self.authenticated = True
        self.driver.close()

    def quit(self):
        self.driver.quit()
        self.session.close()

    # TODO (Tim): Implement a deep copy of all cookie attributes
    def set_session_cookies_from_driver(self):
        driver_cookies = self.driver.get_cookies()
        for cookie in driver_cookies:
            self.session.cookies.set(cookie['name'], cookie['value'])

    def load_page(self, page_number):
        url = 'https://airlines.invoiceworks.net/new/XMLRequestHandler.aspx?'
        payload = {
            'req': 'LoadPage',
            'Page': page_number
        }
        r = self.session.get(url, params=payload, timeout=5)
        r.raise_for_status()


class IWArchiveBot(IWBot):
    """A customized `IWBot` object used for https://archive.invoiceworks.net.
    """

    def __init__(self):
        self.authenticated = False
        self.driver = webdriver.Ie()
        self.session = requests.Session()


    def connect(self):
        """Override `IWBot.start_session` to authenticate with
        https://archive.invoiceworks.net.
        """

        sso_url = 'https://www.hellojetblue.com/resources/invoiceworks'
        archive_sso_url = 'https://airlines.invoiceworks.net/new/InvoiceWorks.aspx?page=103'
        self.driver.get(sso_url)
        self.driver.get(archive_sso_url)

        # Clunky way to check for error page on sign-in
        i = 0
        while self.driver.title == 'IWError' and i < 5:
            self.driver.get(archive_sso_url)
            i += 1
        if self.driver.title == 'IWError' and i == 4:
            raise ConnectionError('Could not establish connection with https://archive.invoiceworks.net.')
            
        self.set_session_cookies_from_driver()
        self.authenticated = True
        self.driver.close()

    def load_page(self, page_number):
        url = 'https://archive.invoiceworks.net/archive_C0000115/XMLRequestHandler.aspx'
        payload = {
            'req': 'LoadPage',
            'Page': page_number
        }
        r = self.session.get(url, params=payload, timeout=5)
        r.raise_for_status()
        return r

    def export_report_as_csv(self, report_id):
        url = 'https://archive.invoiceworks.net/archive_C0000115/InvoiceWorks.aspx?'
        payload = {
            'page': 95,
            'action': 'ExportCSV',
            'ReportID': report_id
        }
        self.verify_ssl = False
        r = self.session.get(url, params=payload, timeout=30)
        r.raise_for_status()
        return r


            

if __name__ == "__main__":
    print('Nothing implemented yet.')